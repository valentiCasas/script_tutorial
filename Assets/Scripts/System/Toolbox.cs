﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Toolbox : Singleton<Toolbox> {

	protected Toolbox () {} // guarantee this will be always a singleton only - can't use the constructor!

	void Awake () 
	{
		// Your initialization code here
	}

	public void Show(string text, Text textOut = null, bool add = false)
	{
		if (textOut == null) 
		{
			Debug.Log (text);
		} 
		else 
		{
			if (add)
				textOut.text += "\n" + text;
			else
				textOut.text = text;
		}
	}

	public void NextScene ()
	{
		// It will load the next scene in build settings, if there is no next scene it will load the first one
		int nextSceneIndex = SceneManager.GetActiveScene ().buildIndex;
		nextSceneIndex = (nextSceneIndex + 1) % SceneManager.sceneCountInBuildSettings;

		SceneManager.LoadScene (nextSceneIndex, LoadSceneMode.Single);
	}
	
	// (optional) allow runtime registration of global objects
	static public T RegisterComponent<T> () where T: Component 
	{
		return Instance.GetOrAddComponent<T>();
	}
}
