﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorHUD : MonoBehaviour {

	// Publica
	public Text vida;
	public Text champis;
	public Text exp;

	// Privada
	private Jugador jugador;

	void Start () 
	{
		// Creamos un objeto de la classe Jugador
		jugador = new Jugador ();	
	}

	void ActualizarHUD ()
	{
		vida.text = "x" + jugador.Vidas;
		champis.text = "x" + jugador.Champis;
		exp.text = jugador.Experiencia + " XP";
	}

	void Update () 
	{
		ActualizarHUD();
	}




	public void SumarVida()
	{
		jugador.Vidas++;
	}

	public void RestarVida()
	{
		if (jugador.Vidas > 0) 
		{
			jugador.Vidas--;
		}
	}

	public void SumarChampi()
	{
		jugador.Champis++;
	}

	public void RestarChampi()
	{
		if (jugador.Champis > 0) 
		{
			jugador.Champis--;
		}
	}

	public void SumarExp()
	{
		jugador.Experiencia += 10;
	}

	public void RestarExp()
	{
		if (jugador.Experiencia > 0) 
		{
			jugador.Experiencia -= 10;
		}
	}
}
