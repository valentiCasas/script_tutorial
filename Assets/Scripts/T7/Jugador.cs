﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador
{
	// https://unity3d.com/es/learn/tutorials/topics/scripting/classes

	public int Vidas;		// Contador de vidas
	public int Champis;		// Contador de Champis
	public int Experiencia;	// Contador de Experiencia

	// Funcion constructora por defecto
	public Jugador()
	{
		Vidas = 10;
		Champis = 0;
		Experiencia = 0;
	}

	// Funcion constructora con parámetros
	public Jugador (int vidas, int champis, int experiencia)
	{
		Vidas = vidas;
		Champis = champis;
		Experiencia = experiencia;
	}
}
