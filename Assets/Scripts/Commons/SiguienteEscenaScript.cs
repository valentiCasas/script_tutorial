﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SiguienteEscenaScript : MonoBehaviour {

	void Start () 
	{
		Button btn = gameObject.GetComponent<Button>();
		btn.onClick.AddListener (SiguienteEscena);
	}
	
	void SiguienteEscena()
	{
		Toolbox.Instance.NextScene ();
	}
}
