﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Activador : MonoBehaviour {

	public GameObject objeto;

	void Start () 
	{
		Toggle compActivador = GetComponent<Toggle> ();
		compActivador.onValueChanged.AddListener ((bool valor) => activador (compActivador.isOn));
	}

	void activador (bool activar)
	{
		objeto.SetActive (activar);
	}

}
