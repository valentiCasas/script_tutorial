﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AWAKE_START : MonoBehaviour {

	// https://unity3d.com/es/learn/tutorials/topics/scripting/awake-and-start?playlist=17117
	// https://unity3d.com/es/learn/tutorials/topics/scripting/enabling-and-disabling-components?playlist=17117
	// https://unity3d.com/es/learn/tutorials/topics/scripting/activating-gameobjects?playlist=17117

	public GameObject textoUI;

	private Toggle activador;
	private Text texto;

	// Funcion que se llama 1 unica vez automaticamente al cargar el script, 
	// Awake es la primera en llamarse incluso aunque el Componente Script esté deshabilitado
	// Se usa principalmente para configurar referencias entre scripts y para inicializaciones
	void Awake()
	{
		// Deshabilitamos este Componente de tipo Script
		enabled = false;

		// Obtenemos el Componente de tipo Toggle del objeto actual
		activador = GetComponent<Toggle> ();
		activador.onValueChanged.AddListener ((bool valor) => habilitador (activador.isOn));

		// Obtenemos el Componente de tipo Text del objeto textoUI
		texto = textoUI.GetComponent<Text> ();

		// Deshabilitamos el componente Text, le cambiamos el color y el texto
		texto.color = Color.red;
		texto.text = "En el metodo Awake el texto se tinta a rojo";
	}

	// Funcion que se llama 1 unica vez automaticamente al cargar el script,
	// Start se llama despues de Awake pero justo antes de llamar al primer Update, aunque solamente si el Componente Script está habilitado
	// Se usa principalmente para inicializar cualquier parte del script que se necesite cuando este este habilitado
	void Start () 
	{
		texto.color = Color.green;
		texto.text = "En el metodo Start el texto se tinta verde";
	}
		
	void habilitador (bool habilitado)
	{
		enabled = habilitado;
	}

}
