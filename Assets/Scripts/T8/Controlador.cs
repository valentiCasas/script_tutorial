﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class Controlador : MonoBehaviour {

	public float velocidadMaxima; 	// Velocidad máxima del personaje
	public float fuerzaDeSalto;		// Fuerza del salto del personaje

	public Transform chequeaSuelo;	// Componente Transform del objeto que se usa para comprobar que el personaje está en el suelo
	public LayerMask capaSuelo;		// Componente de Unity que nos permite elegir las "Layers" con las que interactuar (Edit -> Project Settings -> Physics 2D)

	private bool mirandoIzquierda = false;	// Variable para saber en que dirección mira el personaje
	private bool enSuelo = true;			// Variable para saber si está en el suelo
	private float radio = 0.2f;				// Variable usada para comprobar la distancia del suelo al objeto

	private Rigidbody2D rb2D;	// Componente RigibidBody2D
	private Animator anim;		// Componente Animator

	void Start() 
	{
		rb2D = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator> ();
	}

	/* 
	 * Se llama en cada Frame
	 * Se suele usar para: 
	 *    Mover objetos sin fisicas
	 *    Contadores o Timers simples
	 *    Recibir información
	 * Hay que tener en cuenta que el intervalo entre llamadas a la función puede variar
	 */

	void Update() 
	{
		// Se comprueba que el PJ esté en el suelo y si se ha apretado el botón "Jump" (Mirar Edit -> Project Settings -> Input)
		if (enSuelo && Input.GetButtonDown("Jump")) 
		{
			anim.SetBool ("Suelo", false); // Cambiamos el valor de la variable del componente Animator
			rb2D.AddForce (new Vector2 (0.0f, fuerzaDeSalto));	// Le añadimos fuerza al RigidBody2D en dirección de la Y
		}
	}

	/*
	 * Se llama en cada paso (unidad especifica) de físicas
	 * Los intervalos en que se llama no varian a diferencia del Update normal
	 * Se usa para actualizar regularmente los ajustes de objetos con físicas (Rigidbody) 
	 */
	void FixedUpdate() 
	{
		// Comprueba si desde la posicion "chequeaSuelo.position" en un radio de distancia "radio" hay algun objeto de la capa "capaSuelo"
		enSuelo = Physics2D.OverlapCircle (chequeaSuelo.position, radio, capaSuelo);

		// Dependiendo del resultado de la función anterior, se modifica el valor de la variable del Animator
		anim.SetBool ("Suelo", enSuelo);

		// Solamente moveremos el PJ si esta en el suelo
		if (enSuelo) 
		{
			MovimientoHorizontal ();
		}
	}

	void MovimientoHorizontal()
	{
		// Movimiento valdrá entre -1 i 1 dependiendo del control que pulsemos, donde  -1 va a la izquierda y 1 a la derecha
		float movimiento = Input.GetAxis ("Horizontal");

		// Enviamos la velocidad al Animator para controlar la animación
		anim.SetFloat("Velocidad", Mathf.Abs(movimiento));

		// Se le asigna velocidad horizontal al cuerpo 
		rb2D.velocity = new Vector2 (velocidadMaxima * movimiento, rb2D.velocity.y);

		if (movimiento > 0 && mirandoIzquierda) 
		{
			Girar ();
		}
		else if (movimiento < 0 && !mirandoIzquierda)
		{
			Girar();
		}
	}

	void Girar()
	{
		mirandoIzquierda = !mirandoIzquierda;

		// Invertimos la escala de las X para que se gire el objeto y no necesitar el doble de animaciones
		Vector3 escala = transform.localScale;
		escala.x *= -1;
		transform.localScale = escala;
	}

}
