﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LOOKAT : MonoBehaviour {

	// https://unity3d.com/es/learn/tutorials/topics/scripting/look?playlist=17117

	public Transform posicionObjeto; // Componenente Transform del objeto que queremos mirar
	public float intervaloDeTiempo;  // Intervalo de tiempo que tarda a mirar a la nueva posicion

	// Privado
	float tiempoAcumulado; // Variable para acumular el tiempo que ha pasado

	void Start () 
	{
		tiempoAcumulado = 0.0f;
	}
	
	void Update () 
	{
		// Acumulamos el tiempo en segundos que se ha tardado en completar el último frame
		tiempoAcumulado += Time.deltaTime;

		// Cuando acumulamos tantos o más segundos que los que indica el "intervaloDeTiempo"
		if (tiempoAcumulado >= intervaloDeTiempo) 
		{
			// Hace que el objeto se oriente hacia la posicionObjeto que le hemos indicado
			transform.LookAt (posicionObjeto);
		}
	}
}
