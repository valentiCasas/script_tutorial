﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TRANSFORM : MonoBehaviour {

	// https://unity3d.com/es/learn/tutorials/topics/scripting/translate-and-rotate?playlist=17117

	public float moveSpeed = 10f;
	public float turnSpeed = 500f;

	void Update ()
	{
		if (Input.GetKey (KeyCode.UpArrow)) 
		{
			// Desplaza el objeto en sentido del Vector2.up, en este caso hacia arriba, la (distancia moveSpeed * Time.deltaTime)
			transform.Translate (Vector2.up * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.DownArrow)) 
		{
			// Desplaza el objeto en sentido del Vector2.down, en este caso hacia abajo, la (distancia moveSpeed * Time.deltaTime)
			transform.Translate (Vector2.down * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
			// Rota el objeto sobre su eje, en este caso Vector2.left es el eje de las X, una cantidad (turnSpeed * Time.deltaTime)
			transform.Rotate (Vector2.left, turnSpeed * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.RightArrow)) 
		{
			// Rota el objeto sobre su eje, en este caso Vector2.right es el eje de las X también, una cantidad (turnSpeed * Time.deltaTime)
			transform.Rotate (Vector2.right, turnSpeed * Time.deltaTime);
		}
	}
}
