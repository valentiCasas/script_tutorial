﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Declaracion_IF : MonoBehaviour {

	// https://unity3d.com/es/learn/tutorials/topics/scripting/if-statements?playlist=17117

	public Text temperatura;
	public Text prueba;

	float temperaturaCafe = 85.0f;
	float temperaturaLimiteCalentar = 70.0f;
	float temperaturaLimiteEnfriar = 40.0f;

	void Update () 
	{
		// Comprueba si se pulsa la tecla ESPACIO
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			PruebaTemperatura();
		}

		// Cada SEGUNDO reduce la temperatura en 1 grado
		temperaturaCafe -= Time.deltaTime * 1.0f;
		Toolbox.Instance.Show (temperaturaCafe.ToString(), temperatura);
	}

	void PruebaTemperatura()
	{
		// Si la temperatura del cafe es mayor que la limite de calentar
		if (temperaturaCafe > temperaturaLimiteCalentar) 
		{
			Toolbox.Instance.Show ("El cafe esta muy caliente", prueba);
		}
		// Si no asi, pero la temperatura del cafe es inferior a la limite de enfriar
		else if (temperaturaCafe < temperaturaLimiteEnfriar) 
		{
			Toolbox.Instance.Show ("El cafe esta demasiado frio", prueba);
		} 
		// Si no es ninguna de las anteriores entonces...
		else 
		{
			Toolbox.Instance.Show ("El cafe esta al punto", prueba);
		}
	}
}
