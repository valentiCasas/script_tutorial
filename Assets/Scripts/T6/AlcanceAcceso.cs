﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlcanceAcceso : MonoBehaviour {

	// https://unity3d.com/es/learn/tutorials/topics/scripting/scope-and-access-modifiers?playlist=17117

	// Public
	public Text salida;

	// Privado		
	private Coche miCoche;	
	private Coche miSegundoCoche;
	
	void Start ()
	{
		// Creamos un objecto de la clase "T3_Coche" y utlizamos sus variables públicas y sus funciones
		miCoche = new Coche();
		miCoche.salida = salida;
		miCoche.MostrarCoche();

		miSegundoCoche = new Coche ("4566GTE", "Seat", "Leon");
		miSegundoCoche.salida = salida;
		miSegundoCoche.MostrarCoche ();
	}

}
