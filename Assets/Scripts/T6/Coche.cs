using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Coche
{
	// https://unity3d.com/es/learn/tutorials/topics/scripting/classes

	public Text salida;

	public string matricula;
	public string marca;		
	public string modelo;

	// Constructor (o Funcion Constructor)
	public Coche()
	{
		this.matricula = "";
		this.marca = "";
		this.modelo = "";
	}

	// Constructor con parámetros
	public Coche(string matricula, string marca, string modelo)
	{
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
	}

	// Funciona que mostrará la información guardada del coche
	public void MostrarCoche ()
	{
		string cadenaSalida = "Matricula: " + matricula + "\nMarca: " + marca + "\nModelo: " + modelo + "\n";
		Toolbox.Instance.Show (cadenaSalida, salida, true);
	}

}

