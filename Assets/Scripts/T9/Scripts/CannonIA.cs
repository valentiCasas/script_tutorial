﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CannonIA : MonoBehaviour {

	public GameObject bola;

	private float m_retraso = 1.5f;
	private Animator m_anim;
	private Vector2 m_posBola;
	private float m_acumulado;

	void Start () 
	{
		m_anim = GetComponent<Animator> ();
		m_posBola = new Vector2 (7.4f, -2.352f);
		m_acumulado = 0.0f;
	}

	void Update () 
	{
		Disparar ();
	}

	void Disparar()
	{
		m_acumulado += Time.deltaTime;

		if (m_acumulado >= m_retraso) 
		{
			m_acumulado = 0.0f;
			m_anim.SetBool ("Disparar", true);
		}
	}

	void Disparo()
	{
		Instantiate (bola, m_posBola, transform.rotation);
	}
}
