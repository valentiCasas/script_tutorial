﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaNormal : MonoBehaviour {

	public float m_velocidadBola = 10.0f;

	private Rigidbody2D m_rb2d;
	private Vector2 m_direccion;

	private float m_duracion = 2.0f;
	private float m_contador = 0.0f;

	void Start () 
	{
		m_rb2d = GetComponent<Rigidbody2D> ();
		m_direccion = new Vector2 (-1.0f, 0.3f); // Angulo de 163 grados aprox
		m_rb2d.velocity = m_direccion * m_velocidadBola;
	}

	void Update () 
	{
		m_contador += Time.deltaTime;

		if (m_contador >= m_duracion)
		{
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "PlayerTag") 
		{
			Destroy (gameObject);
		}
	}

}
