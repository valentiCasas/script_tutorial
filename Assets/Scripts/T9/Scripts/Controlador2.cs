﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class Controlador2 : MonoBehaviour {

	public float velocidadMaxima;
	public float fuerzaDeSalto;

	public Transform chequeaSuelo;
	public LayerMask capaSuelo;		

	public ControladorHUD m_hud;

	// Private
	private bool mirandoIzquierda = false;
	private bool enSuelo = true;			
	private float radio = 0.2f;			

	private Rigidbody2D rb2D;
	private Animator anim;		

	void Start() 
	{
		rb2D = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator> ();
	}

	void Update() 
	{
		if (enSuelo && Input.GetButtonDown("Jump")) 
		{
			anim.SetBool ("Suelo", false); // Cambiamos el valor de la variable del componente Animator
			rb2D.AddForce (new Vector2 (0.0f, fuerzaDeSalto));	// Le añadimos fuerza al RigidBody2D en dirección de la Y
		}
	}

	void FixedUpdate() 
	{
		enSuelo = Physics2D.OverlapCircle (chequeaSuelo.position, radio, capaSuelo);
		anim.SetBool ("Suelo", enSuelo);

		if (enSuelo) 
		{
			MovimientoHorizontal ();
		}
	}

	void MovimientoHorizontal()
	{
		float movimiento = Input.GetAxis ("Horizontal");
		anim.SetFloat("Velocidad", Mathf.Abs(movimiento));

		rb2D.velocity = new Vector2 (velocidadMaxima * movimiento, rb2D.velocity.y);

		if (movimiento > 0 && mirandoIzquierda) 
		{
			Girar ();
		}
		else if (movimiento < 0 && !mirandoIzquierda)
		{
			Girar();
		}
	}

	void Girar()
	{
		mirandoIzquierda = !mirandoIzquierda;

		Vector3 escala = transform.localScale;
		escala.x *= -1;
		transform.localScale = escala;
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "BolaNormalTag") 
		{
			m_hud.RestarVida ();
		}

	}

}
