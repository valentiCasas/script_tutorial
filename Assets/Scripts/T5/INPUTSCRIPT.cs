﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class INPUTSCRIPT : MonoBehaviour {

	public Text salida;
	public Text salida2;

	void Update () 
	{
		string cadena = "";

		bool teclaApretada  = Input.GetKeyDown (KeyCode.S); // Se ha apretado una tecla específica
		bool teclaSostenida = Input.GetKey (KeyCode.S); 	// Se está apretando una tecla específica
		bool teclaSoltada   = Input.GetKeyUp (KeyCode.S);   // Se ha dejado de apretar una tecla específica

		if (teclaApretada) 
		{
			cadena = "Tecla Apretada";
			Toolbox.Instance.Show (cadena, salida, true);
		}
		else if (teclaSostenida) 
		{
			cadena = "Tecla Sostenida";
			Toolbox.Instance.Show (cadena, salida, true);
		}
		else if (teclaSoltada) 
		{
			cadena = "Tecla Soltada";
			Toolbox.Instance.Show (cadena, salida, true);
		}
			
		// Los nombres de los botones estan definidos en el InputManager. Edit --> Project Settings --> Input
		bool botonApretado  = Input.GetButtonDown("Jump"); // Se ha apretado el botón asociado al nombre "Jump"
		bool botonSostenido = Input.GetButton("Jump");	   // Se está apretando el botón asociado al nombre "Jump"
		bool botonSoltado   = Input.GetButtonUp("Jump");   // Se ha soltado el botón asociado al nombre "Jump"

		if (botonApretado) 
		{
			cadena = "Boton Apretado";
			Toolbox.Instance.Show (cadena, salida2, true);
		}
		else if (botonSostenido) 
		{
			cadena = "Boton Sostenido";
			Toolbox.Instance.Show (cadena, salida2, true);
		}
		else if (botonSoltado) 
		{
			cadena = "Boton Soltado";
			Toolbox.Instance.Show (cadena, salida2, true);
		}
	}
}
