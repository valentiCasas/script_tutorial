﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Declaracion_BUCLES : MonoBehaviour {

	// https://unity3d.com/es/learn/tutorials/topics/scripting/loops?playlist=17117

	public Text bucleFor;
	public Text bucleWhile;
	public Text bucleDoWhile;
	public Text bucleForeach;

	void Start()
	{
		// Ejemplo con BUCLE FOR
		int numEnemigos = 3;

		for(int i = 0; i < numEnemigos; i++)
		{
			Toolbox.Instance.Show ("Enemigo numero " + i + " creado", bucleFor, true);
		}

		// Ejemplo con WHILE
		int tazasPorLavar = 4;

		while(tazasPorLavar > 0)
		{
			Toolbox.Instance.Show ("Taza lavada !!", bucleWhile, true);
			tazasPorLavar--;
		}

		// Ejemplo con BUCLE DO WHILE
		int contador = 0;

		do
		{
			Toolbox.Instance.Show ("Hola Mundo !!", bucleDoWhile, true);
			contador++;
			
		}while(contador < 3);

		// Ejemplo con BUCLE FOREACH
		string[] cadenas = new string[] {"Primera cadena", "Segunda cadena", "Tercera cadena"};
				
		foreach(string cadena in cadenas)
		{
			Toolbox.Instance.Show (cadena, bucleForeach, true);
		}
	}
}
